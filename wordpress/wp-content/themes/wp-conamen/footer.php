  </section><!-- /section -->

</div><!-- /wrapper -->

<footer role="contentinfo">
  <div class="inner">

    <div class="footer-block footer-block-left">
      <span class="phone">tel. <a href="tel:+420776260478">+420 776 260 478</a></span>
      <a href="mailto:charita@conamen.cz" class="mail">charita@conamen.cz</a>
      <p class="copyright">
        &copy; 2011 - <?php echo date("Y"); ?> Spolek Conamen<br>Pomoc hendikepovaným dětem
      </p><!-- /copyright -->
    </div><!-- /.footer-block footer-block-left -->

    <div class="footer-block footer-block-middle">
      <?php wpeFootNav(); ?>
    </div><!-- /.footer-block footer-block-middle -->

    <div class="footer-block footer-block-right">
      <p>
        BANKOVNÍ SPOJENÍ<br>
        Raiffeisenbank a.s.<br>
        Hvězdova 1716/2B, 140 78 Praha 4<br>
        Veřejná sbírka<br>
        Číslo účtu: 2223 2224/5500<br>
        BIC/SWIFT: RZBCCZPP<br>
        IBAN: CZ2755000000000022232224<br>
        Provozní účet<br>
        Číslo účtu: 640 840 6001/5500<br>
        BIC/SWIFT: RZBCCZPP<br>
        IBAN: CZ275500000000006408406001
      </p>
    </div><!-- /.footer-block footer-block-right -->

  </div><!-- /.inner -->
</footer><!-- /footer -->

    <?php wp_footer(); ?>

</body>
</html>
